package uepg.paulo.analisar.lexico.pascal;

import java_cup.runtime.*;

%%

%{


private TokenPascal criaToken(String nome, String valor) {
	return new TokenPascal( nome, valor, yyline, yycolumn);
}

%}

%public
%class AnalisadorLexico
%type TokenPascal
%line
%column

inteiro          	= 0|[1-9][0-9]*
brancos			= [\n| |\t|\r]
delimitadores		= (":" | ";" | ".")
identificadores		= [A-Za-z_][A-Za-z_0-9]*

leftbrace       = \{
rightbrace      = \}
comment_body    = {nonrightbrace}*
nonrightbrace   = [^}]
comentario_1	= {leftbrace}{comment_body}{rightbrace}

comentario_2	= "/*" [^*] ~"*/" | "/*" "*"+ "/"

LineTerminator = \r|\n|\r\n
InputCharacter = [^\r|\n|]
comentario_3	= "//" {InputCharacter}* {LineTerminator}

%%

"real"			{ return criaToken( "real", "" ); }
"integer"		{ return criaToken( "integer", "" ); }
">"			{ return criaToken( ">", "" ); }
"*"			{ return criaToken( "*", "" ); }
":="			{ return criaToken( ":=", "" ); }
"program" 	{ return criaToken( "program", "" ); }		
"var"		{ return criaToken( "var", "" ); }
"begin"		{ return criaToken( "begin", "" ); }
"if"		{ return criaToken( "if", "" ); }
"then"		{ return criaToken( "then", "" ); }
"else"		{ return criaToken( "else", "" ); }
"end" 		{ return criaToken( "end", "" ); }


{comentario_1}  { return criaToken("comentário", "");  }
{comentario_2}  { return criaToken("comentário", "");  }
{comentario_3}  { return criaToken("comentário", "");  }


{identificadores}	{ return criaToken( "id", yytext() ); }
{delimitadores}		{ return criaToken( yytext(), "" ); }
{inteiro} 		{ return criaToken( "numero", yytext() ); }
{brancos} 		{ /**/ }

. { System.out.println("Erro sintaxe: ^"+yytext()+"| Linha: "+yyline+"| Coluna: "+yycolumn); }