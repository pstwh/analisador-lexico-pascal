package uepg.paulo.analisar.lexico.pascal;

/**
 *
 * @author paulo
 */

import java.io.FileReader;
import java.io.IOException;
//import java.nio.file.Paths;

public class AnalisadorLexicoPascal {
    
    public static void main(String[] args) throws IOException {
        String codigoFontePascal = "/home/paulo/NetBeansProjects/analisar-lexico-pascal/src/main/java/uepg/paulo/analisar/lexico/pascal/pascal.pas";
        
        AnalisadorLexico l = new AnalisadorLexico(new FileReader(codigoFontePascal));
        
        TokenPascal t;
        
        while ((t = l.yylex()) != null) {
            System.out.println("<"+t.nome+" | "+t.valor+"> ("+t.linha+" - "+t.coluna+")");
        }
        
    }
    
}
