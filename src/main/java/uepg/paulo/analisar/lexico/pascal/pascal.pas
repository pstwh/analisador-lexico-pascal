program ordena;
uses crt;
var
  vetor: array[1..6] of integer;
  aux: integer;
  i,j: integer;
begin
  clrscr;
  for i:=1 to 6 do
  begin
    write('Digite o ', i, 'º número: ');
    readln(i);
  end;
  for i:= 1 to 5 do
  begin
    for j:=1 to 5 do
    begin
      if vetor[j] > vetor[j+1] then
      begin
        aux:=vetor[j];
        vetor[j]:=vetor[j+1];
        vetor[j+1]:=aux;
      end;
    end;
end.