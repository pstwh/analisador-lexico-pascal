package uepg.paulo.analisar.lexico.pascal;

/**
 *
 * @author paulo
 */
public class TokenPascal {
    
    public String nome;
    public String valor;
    public Integer linha;
    public Integer coluna;
    
    public TokenPascal(String nome, String valor, Integer linha, Integer coluna) {
        this.nome = nome;
        this.valor = valor;
        this.linha = linha;
        this.coluna = coluna;
    }
    
}
