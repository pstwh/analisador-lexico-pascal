package uepg.paulo.analisar.lexico.pascal;

/**
 *
 * @author paulo
 */

import java.io.File;
//import java.nio.file.Paths;

public class Gerador {
    public static void main(String[] args) {
        
        String arquivoLexPascal = "/home/paulo/NetBeansProjects/analisar-lexico-pascal/src/main/java/uepg/paulo/analisar/lexico/pascal/pascal.lex";
        
        File codigoPascal = new File(arquivoLexPascal);
        
        jflex.Main.generate(codigoPascal);
    }
}
